import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { CreateComplaintPage } from '../pages/createComplaint/create-complaint';
import { CreateComplaintNamePage } from '../pages/create-complaint-name/create-complaint-name';
import { CreateComplaintInvestigationPage }  from '../pages/create-complaint-investigation/create-complaint-investigation';
import { LogOutPage } from '../pages/logout/logout';
import { Auth } from '../pages/providers/auth';
import { DetailListPage } from '../pages/detail-list/detail-list';
import { ApprovedPage } from '../pages/approved/approved';
import { FlowsPage} from '../pages/flows/flows';
import { ActionlistPage } from '../pages/actionlist/actionlist';

import { DistributionSelectPage } from '../pages/distribution-select/distribution-select';
import { InvestigationCommentPage } from '../pages/investigation-comment/investigation-comment';
import { ValidationNotePage } from '../pages/validation-note/validation-note';
import { CsConfirmSendPage } from '../pages/cs-confirm-send/cs-confirm-send';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    DetailListPage,
    ApprovedPage,
    FlowsPage,
    ActionlistPage,
    LogOutPage,
    CreateComplaintPage,
    CreateComplaintNamePage,
    CreateComplaintInvestigationPage,
    DistributionSelectPage,
    InvestigationCommentPage,
    ValidationNotePage,
    CsConfirmSendPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    DetailListPage,
    ApprovedPage,
    FlowsPage,
    ActionlistPage,
    LogOutPage,
    CreateComplaintPage,
    CreateComplaintNamePage,
    CreateComplaintInvestigationPage,
    DistributionSelectPage,
    InvestigationCommentPage,
    ValidationNotePage,
    CsConfirmSendPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Auth
  ]
})
export class AppModule {}
