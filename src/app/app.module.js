var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { CreateComplaintPage } from '../pages/createComplaint/create-complaint';
import { CreateComplaintNamePage } from '../pages/create-complaint-name/create-complaint-name';
import { CreateComplaintInvestigationPage } from '../pages/create-complaint-investigation/create-complaint-investigation';
import { LogOutPage } from '../pages/logout/logout';
import { Auth } from '../pages/providers/auth';
import { DetailListPage } from '../pages/detail-list/detail-list';
import { ApprovedPage } from '../pages/approved/approved';
import { DistributionPage } from '../pages/distribution/distribution';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            MyApp,
            HomePage,
            ListPage,
            LoginPage,
            DetailListPage,
            ApprovedPage,
            LogOutPage,
            CreateComplaintPage,
            CreateComplaintNamePage,
            CreateComplaintInvestigationPage,
            DistributionPage
        ],
        imports: [
            BrowserModule,
            IonicModule.forRoot(MyApp),
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            MyApp,
            HomePage,
            ListPage,
            LoginPage,
            DetailListPage,
            ApprovedPage,
            LogOutPage,
            CreateComplaintPage,
            CreateComplaintNamePage,
            CreateComplaintInvestigationPage,
        ],
        providers: [
            StatusBar,
            SplashScreen,
            { provide: ErrorHandler, useClass: IonicErrorHandler },
            Auth
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map