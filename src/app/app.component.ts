import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ActionlistPage } from '../pages/actionlist/actionlist';
import { LoginPage } from '../pages/login/login';
import { CreateComplaintPage } from '../pages/createComplaint/create-complaint';

import { LogOutPage } from '../pages/logout/logout';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{icon: string, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {

    this.initializeApp();

    this.pages = [
      { icon:'logo-windows', title: 'Home', component: HomePage },
      { icon:'add', title: 'New Complaint', component: CreateComplaintPage},
      { icon:'list', title: 'List Complaint', component: ListPage },
      { icon:'checkbox', title: 'Action List Complaint', component: ActionlistPage},
      { icon:'log-out', title: 'LogOut' , component:LogOutPage}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
