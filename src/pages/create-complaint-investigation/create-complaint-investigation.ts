import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Auth } from '../providers/auth';


@Component({
  selector: 'page-create-complaint-investigation',
  templateUrl: 'create-complaint-investigation.html',
})
export class CreateComplaintInvestigationPage {
	urlApi="http://192.168.105.20:8080";
  complaintDetail : any;
	visitResult : any;
  dataitems;
  employID;
  dtEmploy: any;
  filename;
  filesToUpload: Array<File> = [];
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public http: Http,
    private authService: Auth
  	) {
  	this.getDataitems();
    this.getDataUser();
  }

  getDataUser(){
    this.authService.getProfile().subscribe(profile =>{
        this.employID = profile.user.nEmployeeID;
        this.dtEmploy = profile.user;
    });
  }

  getDataitems() {
    // Item
      this.http.get(this.urlApi + '/dataitem').map(res => res.json()).subscribe(datas => {
        this.dataitems = datas;
      });
  }

  upload() {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      const formData: any = new FormData();
      const files: Array<File> = this.filesToUpload;
     
      for(let i =0; i < files.length; i++){
          formData.append("uploads[]", files[i], files[i]['name']);
      }
      // formData.append("uploads[]", files[0], files[0]['name']);
      //this.address.documents = files.toString();
      var datafile = {
        nTransactionID:this.navParams.get('ntransnumber'),
        dProcessDate:this.navParams.get('TxtDate')
      }
      this.http.post(this.urlApi + '/transnum', datafile, options)
       .map(res => res.json()).subscribe(data => {
       });

      this.http.post(this.urlApi +'/upload', formData, 'xxx')
      .map(res => res.json())
      .subscribe(datas => {
        console.log(datas[0]);
        this.filename=datas;
      });
  }

  fileChangeEvent(fileInput: any) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
      this.upload();
      //this.product.photo = fileInput.target.files[0]['name'];
  }


  onSubmitComplaint(){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

  	// let data = {
  	// 	TxtEmployeeID : 1,
  	// 	  TxtNumber : this.navParams.get('ntransnumber'),
   //      TxtDate : this.navParams.get('TxtDate'),
   //      CmbCsName : this.navParams.get('CmbCsName'),
   //      TxtBatch : this.navParams.get('TxtBatch'),
   //      TxtExpired :this.navParams.get('TxtExpired'),
   //      CmbComplaintType : this.navParams.get('CmbComplaintType'),
   //      CmbCrop : this.navParams.get('CmbCrop'),
   //      CmbVarietas : this.navParams.get('CmbVarietas'),
   //      CmbQty : this.navParams.get('CmbQty'),
   //      TxtQty : this.navParams.get('TxtQty'),
   //      CmbContent : this.navParams.get('CmbContent'),
   //      TxtContent : this.navParams.get('TxtContent'),
   //      TxtInspName : this.navParams.get('TxtInspName'), 
   //      TxtInspPhone : this.navParams.get('TxtInspPhone'), 
   //      TxtInspMail : this.navParams.get('TxtInspMail'), 
   //      TxtCustName : this.navParams.get('TxtCustName'), 
   //      TxtCustPhone : this.navParams.get('TxtCustPhone'),  
   //      CmbCustCountry : this.navParams.get('CmbCustCountry'), 
   //      CmbCustProvince :  this.navParams.get('CmbCustProvince'),
   //      CmbCustRegency : this.navParams.get('CmbCustRegency'),
   //      TxtCustAddress : this.navParams.get('TxtCustAddress'),
   //      TxtInvestigationDetail : this.complaintDetail,
   //      TxtInvestigationVisit : this.visitResult
  	// };

    console.log(this.dtEmploy);

    var datatransheader: any = {
        dtemploy:this.dtEmploy,
        dtrans:{
          nTransactionID: this.navParams.get('ntransnumber'),
          nEmployeeID: this.employID,
          nEmployeeCSID: this.navParams.get('CmbCsName'),
          cTransactionNo: this.navParams.get('ntransnumber'),
          dTransactionDate: this.navParams.get('TxtDate'),
          cCustomerName:this.navParams.get('TxtCustName'),
          cCustomerAddress: this.navParams.get('TxtCustAddress'),
          nCountry: this.navParams.get('CmbCustCountry'),
          nProvince: this.navParams.get('CmbCustProvince'),
          nCounty: this.navParams.get('CmbCustRegency'),
          cCustomerPhoneNo: this.navParams.get('TxtCustPhone'),
          cInspectorName: this.navParams.get('TxtInspName'),
          cInspectorPhoneNo: this.navParams.get('TxtInspPhone'),
          cInspectorEmail: this.navParams.get('TxtInspMail'),
          cTransactionDescription: this.complaintDetail,
          cTransactionFirstResult: this.visitResult,
          bTransactionStatus: 1,
          dInsertDate: this.navParams.get('TxtDate'),
          
        },

        // quantity detail
        detquantity: [
          {
            nTransactionID: this.navParams.get('ntransnumber'),
            nTransactionUnitID: this.navParams.get('CmbQty'),
            nTransactionValue: this.navParams.get('TxtQty'),
            dProcessDate: this.navParams.get('TxtDate'),
          },
          {
            nTransactionID: this.navParams.get('ntransnumber'),
            nTransactionUnitID: this.navParams.get('CmbContent'),
            nTransactionValue: this.navParams.get('TxtContent'),
            dProcessDate: this.navParams.get('TxtDate'),
          }
        ],

        // data item
        dataItems: {
          nTransactionID: this.navParams.get('ntransnumber'),
        nItemID: this.dataitems,
        cTransactionValue: [this.navParams.get('TxtBatch'),
            this.navParams.get('TxtExpired'),
            this.navParams.get('CmbCrop'),
            this.navParams.get('CmbVarietas')
        ],
        cTransactionDetailNotes: null,
        dProcessDate: this.navParams.get('TxtDate')
        }
        
      };

      this.http.post(this.urlApi + '/complaintheader', datatransheader, options)
      .map(res => res.json())
      .subscribe(data => {
        console.log(data);
      });

  }

  ionViewDidLoad() {
    
  }

}
