import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';

@Component({
  selector: 'page-logout',
  template: ''
})
export class LogOutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.onLogout();
  }

  onLogout(){
    this.navCtrl.setRoot(LoginPage);
  }

}
