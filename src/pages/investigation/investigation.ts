import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { InvestigationCommentPage } from '../investigation-comment/investigation-comment';

/**
 * Generated class for the InvestigationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-investigation',
  templateUrl: 'investigation.html',
})
export class InvestigationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvestigationPage');
  }

  onInvestComment(){
  	this.navCtrl.push(InvestigationCommentPage);
  }
}
