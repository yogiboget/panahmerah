import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()

export class Auth {
  authToken;
  user;
  options;
  urlApi="http://192.168.105.20:8080";

  constructor(
    public http: Http
  ) { }

  createAuthenticationHeaders() {
    this.loadToken(); 
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    });
  }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  loginUser(user) {
    return this.http.post(this.urlApi+'/users/login', user)
    .map(res => res.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user)); 
    this.authToken = token;
    this.user = user;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  getProfile() {
    this.createAuthenticationHeaders();
    return this.http.get(this.urlApi+'/users/profile', this.options)
    .map(res => res.json());
  }

  loggedIn() {
    return tokenNotExpired();
  }
}