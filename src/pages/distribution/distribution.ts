import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DistributionSelectPage } from '../distribution-select/distribution-select';
/**
 * Generated class for the DistributionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-distribution',
  templateUrl: 'distribution.html',
})
export class DistributionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DistributionPage');
  }

  onDistribution(){
	this.navCtrl.push(DistributionSelectPage);
  }

}
