import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/Rx';

import { DetailListPage } from '../../pages/detail-list/detail-list';
import { ApprovedPage } from '../../pages/approved/approved';
import { FlowsPage} from '../../pages/flows/flows';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  lisComplaint:any;
  filYear:any;
  filOption:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http
    ) {
    this.filYear = new Date().getFullYear();
    this.filOption =0; 
  }
   
  ionViewDidLoad() {
      //this.getListComplaint();
      
  }

  getListComplaint(){
    this.http.get('http://shared.panahmerah.id/complaint/php/view/ViewGridListComplaint.php?nEmployeeID=1&nVwOption=0&nYear=2017&page=1&start=0&limit=10')
    .map(res=>res.json()).subscribe(data=>{
      this.lisComplaint = data.data;
      console.log(this.lisComplaint);
    })
  }

  showDetail(){
    this.navCtrl.push(DetailListPage);
  }

  showWorkFlow(){
    this.navCtrl.push(FlowsPage);
  }

}
