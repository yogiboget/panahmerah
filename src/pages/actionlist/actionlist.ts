import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/Rx';

import { DetailListPage } from '../../pages/detail-list/detail-list';
import { FlowsPage} from '../../pages/flows/flows';
import { ApprovedPage } from '../../pages/approved/approved';
import { DistributionSelectPage } from '../../pages/distribution-select/distribution-select';
import { InvestigationCommentPage } from '../../pages/investigation-comment/investigation-comment';
import { ValidationNotePage } from '../../pages/validation-note/validation-note';

@IonicPage()
@Component({
  selector: 'page-actionlist',
  templateUrl: 'actionlist.html',
})
export class ActionlistPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http
    ) {  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ActionlistPage');
  }

  showDetail(){
    this.navCtrl.push(DetailListPage);
  }

  showWorkFlow(){
    this.navCtrl.push(FlowsPage);
  }

  actionGo(){
    // this.navCtrl.push(ApprovedPage);
    // this.navCtrl.push(DistributionSelectPage);
    // this.navCtrl.push(InvestigationCommentPage);
    this.navCtrl.push(ValidationNotePage);
  }

}
