import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { CreateComplaintInvestigationPage } from '../create-complaint-investigation/create-complaint-investigation';


import { Http } from '@angular/http';
import 'rxjs/Rx';

@Component({
  selector: 'page-create-complaint-name',
  templateUrl: 'create-complaint-name.html',
})
export class CreateComplaintNamePage {

  urlApi="http://192.168.105.20:8080";
  nationalitys;
  marketofs;
  dataitems;
  modatas;
  regrencys;
  momarket;
  nationality;
  regrency;


	countrys: any;
	provinces: any;
	regencys: any;
  address: any;
  province: any;
  regency: any;

  InspName : any;
  InspPhone : any;
  InspMail : any;
  CustName : any;
  CustPhone : any;

	loading: any;

	country = 96;

 	constructor(
     public navCtrl: NavController, 
     public navParams: NavParams, 
     public http: Http, 
     public loadingCtrl: LoadingController
  ) {

 		this.http=http;

 		this.loading = this.loadingCtrl.create({
  			content: 'Please wait...'
  		});

  		this.getDataAPI();

  		// this.onChange(96);
  	}

  	getDataAPI() {

      // Modal 2
      this.http.get(this.urlApi + '/nationality').map(res => res.json()).subscribe(data => {
        this.nationalitys = data;
      });
      this.http.get(this.urlApi + '/user/mo').map(res => res.json()).subscribe(data => {
        this.marketofs = data;
      });

      // Item
      this.http.get(this.urlApi + '/dataitem').map(res => res.json()).subscribe(datas => {
        this.dataitems = datas;
      });

  		// this.loading.present();
  		// // countrys
  		// this.http.get('http://shared.panahmerah.id/complaint/php/function/LoadComboData.php?nParam=4&nParentID=0&cText=').map(res => res.json()).subscribe(data => {
	   //      this.countrys = data.data;
	   //  }, 
	   //  err => {
	   //  	console.log("Error : " + err);
	   //  },
	   //  () => {
	   //  	this.loading.dismiss();
	   //  });
  	}

    getDtMo(moId){
      console.log(moId);
      this.http.get(this.urlApi + '/user/mo/'+ moId.nEmployeeID).map(res => res.json()).subscribe(data => {
          this.modatas = data[0].cUserEmail;
      });
    }

    
    getProv(countryID) {
      this.http.get(this.urlApi + '/nationalitylevel/'+countryID).map(res => res.json()).subscribe(data => {
          this.provinces = data;
      });
    }

     getReg(provID) {
      this.http.get(this.urlApi + '/nationalityarea/'+ provID).map(res => res.json()).subscribe(data => {
          this.regrencys = data;
      });
    }

    keComplainNavigation(){
      //kirim variabel
      this.navCtrl.push(CreateComplaintInvestigationPage, {
        ntransnumber : this.navParams.get('ntransnumber'),
        TxtDate : this.navParams.get('TxtDate'),
        CmbCsName : this.navParams.get('CmbCsName'),
        TxtBatch : this.navParams.get('TxtBatch'),
        TxtExpired :this.navParams.get('TxtExpired'),
        CmbComplaintType : this.navParams.get('CmbComplaintType'),
        CmbCrop : this.navParams.get('CmbCrop'),
        CmbVarietas : this.navParams.get('CmbVarietas'),
        CmbQty : this.navParams.get('CmbQty'),
        TxtQty : this.navParams.get('TxtQty'),
        CmbContent : this.navParams.get('CmbContent'),
        TxtContent : this.navParams.get('TxtContent'),
        TxtInspName : this.momarket.cUserName, 
        TxtInspPhone : this.InspPhone, 
        TxtInspMail : this.modatas, 
        TxtCustName : this.CustName, 
        TxtCustPhone : this.CustPhone,  
        CmbCustCountry : this.nationality, 
        CmbCustProvince :  this.province,
        CmbCustRegency : this.regrency,
        TxtCustAddress : this.address
      });
     }

}
