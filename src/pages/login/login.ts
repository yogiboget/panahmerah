import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { Auth } from '../providers/auth';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username;
  password;
  loading: any;

  constructor(
  	public menu: MenuController,
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public auth: Auth,
  	public alertCtrl: AlertController,
  	public loadingCtrl: LoadingController
  ) {
  	this.menu.swipeEnable(false, 'menu1');
  	this.loading = this.loadingCtrl.create({
		content: 'Please wait...'
	});
  }

  onLoginSubmit() {
    this.navCtrl.setRoot(HomePage);
  // 	this.loading.present();
  //   const user = {
  //     username : this.username,
  //     password : this.password
  //   };
  //   this.auth.loginUser(user).subscribe(data => {
  //     if(!data.success){
  //       let alert = this.alertCtrl.create({
	 //      title: 'Info!',
	 //      subTitle: data.message,
	 //      buttons: ['OK']
	 //    });
	 //    alert.present();
	 //    this.loading.dismiss();
  //     }else{
  //       this.auth.storeUserData(data.token, data.user);
  //       this.navCtrl.setRoot(HomePage);
  //       this.loading.dismiss();
  //     }
  //   })
  }
}
