import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { CreateComplaintPage } from '../createComplaint/create-complaint';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	constructor(public menu:MenuController, public navCtrl: NavController) {
		this.menu.swipeEnable(true, 'menu1');
  	}

  	onComplaint(){
  		this.navCtrl.push(CreateComplaintPage);
  	}
}
