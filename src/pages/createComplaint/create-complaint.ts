import { Component, Input } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/Rx';


import { CreateComplaintNamePage } from '../create-complaint-name/create-complaint-name';

@Component({
  selector: 'page-create-complain',
  templateUrl: 'create-complaint.html'
})

export class CreateComplaintPage {

	// complaintName = CreateComplaintNamePage;

	urlApi = 'http://192.168.105.20:8080';

	// Variable ngModel 
	ncomplaint;
	dtTransTypes;
	nationalitys;
	marketofs;
	dataitems;
	cdate = Date.now();
	tUnits;
	tUnits1;
	Varietas;
	varietas;
	qty;
	qty1;
	tunit;
	tunit1;
	
	batch:any;
	expired : any;
	
	category: any;
	csnames: any;
	csname: any;
	crops: any;
	//crop : any;
	

	loading: any;

	@Input() crop: any;

	newComplaint : {};
	

  	constructor(
  		public navCtrl: NavController, 
  		public http: Http, 
  		public loadingCtrl: LoadingController
  	) {
  		this.http=http;
  		
  		this.loading = this.loadingCtrl.create({
  			content: 'Please wait...'
  		});

  		this.getDataAPI();
  	}

  	keComplainName(){
  		//kirim variabel
  		this.navCtrl.push(CreateComplaintNamePage, {
  			ntransnumber : this.ncomplaint,
  			TxtDate : this.cdate,
  			CmbCsName : this.csname,
  			TxtBatch : this.batch,
  			TxtExpired :this.expired,
  			CmbComplaintType : this.category,
  			CmbCrop : this.crop.cCropName,
  			CmbVarietas : this.varietas,
  			CmbQty : this.tunit,
  			TxtQty : this.qty,
  			CmbContent : this.tunit1,
  			TxtContent : this.qty1
  		});
  	}

  	getDataAPI() {

  		this.http.get(this.urlApi + '/autocode').map(res => res.json())
		.subscribe(data => {
			this.ncomplaint = data;
		});
	 	this.http.get(this.urlApi + '/transactionType').map(res => res.json()).subscribe(data => {
	  		this.dtTransTypes = data;
	  	});
	  	this.http.get(this.urlApi + '/user/cs').map(res => res.json()).subscribe(data => {
	  		this.csnames = data;
	  	});
	  	this.http.get(this.urlApi + '/crop').map(res => res.json()).subscribe(data => {
	  		this.crops = data;
	  		console.log(data);
	  	});
  	}

  	getTransType(tTypeId) {
  		console.log(tTypeId);
  		this.http.get(this.urlApi + '/transunit/'+tTypeId).map(res => res.json()).subscribe(data => {
	        this.tUnits = data;
	    });
	    this.http.get(this.urlApi + '/transunit1/'+tTypeId).map(res => res.json()).subscribe(data => {
	        this.tUnits1 = data;
	    });
  	}

  	getVarietas(cropId){
  		console.log(cropId);
  		this.http.get(this.urlApi + '/varietas/'+cropId.nCropID).map(res => res.json()).subscribe(data => {
	        this.Varietas = data;
	    });
  	}
}
