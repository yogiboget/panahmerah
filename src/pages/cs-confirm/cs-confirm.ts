import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {CsConfirmSendPage} from '../cs-confirm-send/cs-confirm-send';
/**
 * Generated class for the CsConfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-cs-confirm',
  templateUrl: 'cs-confirm.html',
})
export class CsConfirmPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CsConfirmPage');
  }

  onConfirmSend() {
  	this.navCtrl.push(CsConfirmSendPage);
  }
}
