import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ApprovedPage } from '../../pages/approved/approved';


@Component({
  selector: 'page-detail-list',
  templateUrl: 'detail-list.html',
})
export class DetailListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  actionGo(){
    this.navCtrl.push(ApprovedPage);
  }

}
