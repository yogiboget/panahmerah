webpackJsonp([2],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_approved_approved__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailListPage = (function () {
    function DetailListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DetailListPage.prototype.actionGo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_approved_approved__["a" /* ApprovedPage */]);
    };
    return DetailListPage;
}());
DetailListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-detail-list',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/detail-list/detail-list.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Detail List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-lines>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center"><b>First Insfection</b></ion-item-divider>\n    <ion-item>Inspector Name\n      <ion-note item-end color="dark">18-09-2017</ion-note>\n    </ion-item>\n    <ion-item>Phone#\n        <ion-note item-end color="dark">2</ion-note>\n    </ion-item>\n    <ion-item>Email Addres\n        <ion-note item-end color="dark">1</ion-note>\n    </ion-item>\n    <ion-item>Complaint Detail\n       <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>       \n    </ion-item>\n    <ion-item>Visit Result\n        <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center">Costomer</ion-item-divider>\n    <ion-item>Name\n        <ion-note item-end color="dark">Yogi</ion-note>        \n    </ion-item>\n    <ion-item>Phone\n      <ion-note item-end color="dark">087763456789</ion-note>\n    </ion-item>\n    <ion-item>Address\n      <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>\n    </ion-item>\n    <ion-item>Contry\n      <ion-note item-end color="dark">Jawa Tengah</ion-note>        \n    </ion-item>\n    <ion-item>Provence\n      <ion-note item-end color="dark">Jawa Tengah</ion-note>        \n    </ion-item>\n    <ion-item>Regency\n      <ion-note item-end color="dark">Solo</ion-note>\n    </ion-item>      \n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center">Additional Info</ion-item-divider>\n    <ion-item>Batch\n      <ion-note item-end color="dark">7887</ion-note>\n      <p>ini notes</p>\n    </ion-item>\n    <ion-item>Varietas\n      <ion-note item-end color="dark">7887</ion-note>\n      <p>ini notes</p>\n    </ion-item>\n    <ion-item>File name. txt\n      <button ion-button clear item-end >\n        Open\n      </button>\n    </ion-item>    \n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center">Investigation Result</ion-item-divider>\n    <ion-item>Coment\n      <p text-wrap>ini notes</p>\n    </ion-item>\n    <ion-item>File name. txt\n      <button ion-button clear item-end >\n        Open\n      </button>\n    </ion-item>    \n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center"><b>Validation</b></ion-item-divider>\n    <ion-item>Validation\n      <ion-note item-end color="dark">Valid Internal</ion-note>\n    </ion-item>\n    <ion-item>CPA#\n        <ion-note item-end color="dark">2</ion-note>\n    </ion-item>\n    <ion-item>Proposed Compensation\n        <p text-wrap>ini Proposed Compensation</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center"><b>Compensation</b></ion-item-divider>\n    <ion-item>Date Send\n      <ion-note item-end color="dark">Valid Internal</ion-note>\n    </ion-item>\n    <ion-item>Type\n        <ion-note item-end color="dark">2</ion-note>\n    </ion-item>\n    <ion-item>Recieved Date\n      <ion-note item-end color="dark">Valid Internal</ion-note>\n    </ion-item>\n    <ion-item>TTB#\n        <ion-note item-end color="dark">2</ion-note>\n    </ion-item>\n    <ion-item>Recieved Name\n      <ion-note item-end color="dark">Valid Internal</ion-note>\n    </ion-item>\n    <ion-item>Deskription\n        <p text-wrap>ini Proposed Compensation</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center"><b>Compensation Varietas</b></ion-item-divider>\n    <ion-item>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Varietas <ion-badge text-wrap text-left color="dark">Bawang Mereh</ion-badge></h2>\n        </ion-col>\n        <ion-col col-4>\n          <h2>Qty <ion-badge color="dark">2322</ion-badge></h2>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Package <ion-badge text-wrap text-left color="dark">Domestik</ion-badge></h2>\n        </ion-col>\n        <ion-col col-4>\n          <h2>Amount <ion-badge color="dark">2345</ion-badge></h2>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light" align="center"><b>Corrective and Preventive Action</b></ion-item-divider>\n    <ion-item>CPA Number\n      <ion-note item-end color="dark">18-09-2017</ion-note>\n    </ion-item>\n    <ion-item>Description of the problem (Improvement Potential)\n       <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>       \n    </ion-item>\n    <ion-item>The causes of nonconformities (After Comfirmation)\n        <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>\n    </ion-item>\n    <ion-item>The conformitien imfact (Cost/Rework/etc)\n       <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>       \n    </ion-item>\n    <ion-item>Corrective and Preventive Action\n        <p text-wrap> Complaint detail ini Complaint detail ini Complaint detail ini Complaint detail  </p>\n    </ion-item>\n    <ion-item>Verification of CPA\n        <ion-note item-end color="dark">1</ion-note>\n    </ion-item>\n    <ion-item>File name. txt\n      <button ion-button clear item-end >\n        Open\n      </button>\n    </ion-item>\n  </ion-item-group>\n\n</ion-content>\n\n<!-- <ion-footer>\n  <ion-toolbar>\n    <ion-buttons align="center">\n      <button ion-button outline icon-left color="danger" (click)="actionGo()">\n        <ion-icon name="checkmark"></ion-icon>\n        Actions\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-footer> -->\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/detail-list/detail-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], DetailListPage);

//# sourceMappingURL=detail-list.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__createComplaint_create_complaint__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = (function () {
    function HomePage(menu, navCtrl) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.menu.swipeEnable(true, 'menu1');
    }
    HomePage.prototype.onComplaint = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__createComplaint_create_complaint__["a" /* CreateComplaintPage */]);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <button ion-button menuToggle>\n      <ion-icon ios="ios-menu" md="md-menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h3 style="text-align: center;">Wellcome To Apps Complaint</h3>\n\n  <div style="text-align: center; margin-top: 50%;">\n    <button  ion-button color=\'danger\' (click)="onComplaint()" >New Complaint</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateComplaintPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__create_complaint_name_create_complaint_name__ = __webpack_require__(263);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CreateComplaintPage = (function () {
    function CreateComplaintPage(navCtrl, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        // complaintName = CreateComplaintNamePage;
        this.urlApi = 'http://192.168.105.20:8080';
        this.cdate = Date.now();
        this.http = http;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.getDataAPI();
    }
    CreateComplaintPage.prototype.keComplainName = function () {
        //kirim variabel
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__create_complaint_name_create_complaint_name__["a" /* CreateComplaintNamePage */], {
            ntransnumber: this.ncomplaint,
            TxtDate: this.cdate,
            CmbCsName: this.csname,
            TxtBatch: this.batch,
            TxtExpired: this.expired,
            CmbComplaintType: this.category,
            CmbCrop: this.crop.cCropName,
            CmbVarietas: this.varietas,
            CmbQty: this.tunit,
            TxtQty: this.qty,
            CmbContent: this.tunit1,
            TxtContent: this.qty1
        });
    };
    CreateComplaintPage.prototype.getDataAPI = function () {
        var _this = this;
        this.http.get(this.urlApi + '/autocode').map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.ncomplaint = data;
        });
        this.http.get(this.urlApi + '/transactionType').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.dtTransTypes = data;
        });
        this.http.get(this.urlApi + '/user/cs').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.csnames = data;
        });
        this.http.get(this.urlApi + '/crop').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.crops = data;
            console.log(data);
        });
    };
    CreateComplaintPage.prototype.getTransType = function (tTypeId) {
        var _this = this;
        console.log(tTypeId);
        this.http.get(this.urlApi + '/transunit/' + tTypeId).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.tUnits = data;
        });
        this.http.get(this.urlApi + '/transunit1/' + tTypeId).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.tUnits1 = data;
        });
    };
    CreateComplaintPage.prototype.getVarietas = function (cropId) {
        var _this = this;
        console.log(cropId);
        this.http.get(this.urlApi + '/varietas/' + cropId.nCropID).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.Varietas = data;
        });
    };
    return CreateComplaintPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CreateComplaintPage.prototype, "crop", void 0);
CreateComplaintPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-complain',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/createComplaint/create-complaint.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <button ion-button menuToggle>\n      <ion-icon ios="ios-menu" md="md-menu"></ion-icon>\n    </button>\n    <ion-title>Create New Complaint</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item>\n    <ion-label color="dark" stacked>Complaint Number</ion-label>\n    <ion-input type="number" [(ngModel)]="ncomplaint" name="ncomplaint"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="dark" stacked>Complaint Date</ion-label>\n    <ion-input type="text" name="cdate" [ngModel]="cdate | date:\'shortDate\'" (ngModelChange)="cdate=$event"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="dark" stacked>Batch#</ion-label>\n    <ion-input type="number" [(ngModel)]="batch" name="batch" placeholder="0"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="dark" stacked>Expired</ion-label>\n    <ion-input type="text" [(ngModel)]="expired" name="expired" placeholder="0"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="dark">Category</ion-label>\n    <ion-select [(ngModel)]="category" name="category" col-9 border (ngModelChange)="getTransType($event)"> \n      <ion-option *ngFor="let item of dtTransTypes" value="{{item.nTransactionTypeID}}">{{item.cTransactionTypeName}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="dark" >CS Name</ion-label>\n    <ion-select col-9 [(ngModel)]="csname" name="csname" border>\n      <ion-option *ngFor="let item of csnames" value="{{item.nEmployeeID}}">{{ item.cUserName }}</ion-option>\n    </ion-select>\n  </ion-item>\n\n\n  <ion-item>\n    <ion-label color="dark">Crop</ion-label>\n    <ion-select col-9 [(ngModel)]="crop" name="crop" (ngModelChange)="getVarietas($event)">\n      <ion-option *ngFor="let item of crops" [value]="item">{{item.cCropName}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="dark">Varieties</ion-label>\n    <ion-select col-9 [(ngModel)]="varietas" name="varietas">\n      <ion-option *ngFor="let item of Varietas" value="{{item.cVarietasName}}">{{item.cVarietasName}}</ion-option>\n    </ion-select>\n  </ion-item>\n    \n  <ion-label color="dark" style="margin-bottom: -10px;" padding>Quantity</ion-label>\n  <ion-row>\n    <ion-col  col-8>\n    <ion-select col-12 [(ngModel)]="tunit" name="tunit">\n      <ion-option *ngFor="let item of tUnits" value="{{item.nTransactionUnitID}}">{{item.cTransactionUnitName}}</ion-option>\n    </ion-select>\n    </ion-col>\n    <ion-col>\n      <ion-input type="number" [(ngModel)]="qty" name="qty" placeholder="0"></ion-input>\n    </ion-col>  \n  </ion-row>\n\n  <ion-label color="dark" padding style="margin-bottom: -10px; margin-top: -10px;">Content</ion-label>\n\n  <ion-row>\n    <ion-col col-8>\n    <ion-select col-12 [(ngModel)]="tunit1" name="tunit1" item-end>\n      <ion-option *ngFor="let item of tUnits1" value="{{item.nTransactionUnitID}}">{{item.cTransactionUnitName}}</ion-option>\n    </ion-select>\n    </ion-col>\n    <ion-col col-2>\n      <ion-input type="number" [(ngModel)]="qty1" name="qty1" placeholder="0" item-end></ion-input>\n    </ion-col>  \n  </ion-row>\n  <br>\n  <div>\n    <button ion-button color="danger" type="submit" block (click)="keComplainName()">Next</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/createComplaint/create-complaint.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], CreateComplaintPage);

//# sourceMappingURL=create-complaint.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Auth; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Auth = (function () {
    function Auth(http) {
        this.http = http;
        this.urlApi = "http://192.168.105.20:8080";
    }
    Auth.prototype.createAuthenticationHeaders = function () {
        this.loadToken();
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({
                'Content-Type': 'application/json',
                'authorization': this.authToken
            })
        });
    };
    Auth.prototype.loadToken = function () {
        this.authToken = localStorage.getItem('token');
    };
    Auth.prototype.loginUser = function (user) {
        return this.http.post(this.urlApi + '/users/login', user)
            .map(function (res) { return res.json(); });
    };
    Auth.prototype.storeUserData = function (token, user) {
        localStorage.setItem('token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    Auth.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    Auth.prototype.getProfile = function () {
        this.createAuthenticationHeaders();
        return this.http.get(this.urlApi + '/users/profile', this.options)
            .map(function (res) { return res.json(); });
    };
    Auth.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_3_angular2_jwt__["tokenNotExpired"])();
    };
    return Auth;
}());
Auth = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], Auth);

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = (function () {
    function LoginPage(menu, navCtrl, navParams, auth, alertCtrl, loadingCtrl) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.menu.swipeEnable(false, 'menu1');
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
    }
    LoginPage.prototype.onLoginSubmit = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        // 	this.loading.present();
        //   const user = {
        //     username : this.username,
        //     password : this.password
        //   };
        //   this.auth.loginUser(user).subscribe(data => {
        //     if(!data.success){
        //       let alert = this.alertCtrl.create({
        //      title: 'Info!',
        //      subTitle: data.message,
        //      buttons: ['OK']
        //    });
        //    alert.present();
        //    this.loading.dismiss();
        //     }else{
        //       this.auth.storeUserData(data.token, data.user);
        //       this.navCtrl.setRoot(HomePage);
        //       this.loading.dismiss();
        //     }
        //   })
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/login/login.html"*/'<ion-content class="login-content" padding >\n  <ion-row class="logo-row">\n    \n    <ion-col class="logo">\n      <img src="assets/logo/logopm.png"/>\n    </ion-col>\n \n  </ion-row>\n  <div class="login-box">\n    <form #loginForm="ngForm">\n      <ion-row>\n        <ion-col>\n          <ion-list>\n            \n            <ion-item>\n              <ion-input type="text" placeholder="username" name="username" [(ngModel)]="username"></ion-input>\n            </ion-item>\n          \n            <ion-item>\n              <ion-input type="password" placeholder="password" name="password" [(ngModel)]="password"></ion-input>\n            </ion-item>\n            \n          </ion-list>\n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col class="signup-col">\n          <button ion-button class="submit-btn" full type="submit" (click)="onLoginSubmit()">Login</button>          \n        </ion-col>\n      </ion-row>\n      \n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth__["a" /* Auth */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_detail_list_detail_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_flows_flows__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_validation_note_validation_note__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ActionlistPage = (function () {
    function ActionlistPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
    }
    ActionlistPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ActionlistPage');
    };
    ActionlistPage.prototype.showDetail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_detail_list_detail_list__["a" /* DetailListPage */]);
    };
    ActionlistPage.prototype.showWorkFlow = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_flows_flows__["a" /* FlowsPage */]);
    };
    ActionlistPage.prototype.actionGo = function () {
        // this.navCtrl.push(ApprovedPage);
        // this.navCtrl.push(DistributionSelectPage);
        // this.navCtrl.push(InvestigationCommentPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_validation_note_validation_note__["a" /* ValidationNotePage */]);
    };
    return ActionlistPage;
}());
ActionlistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-actionlist',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/actionlist/actionlist.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <button ion-button menuToggle>\n      <ion-icon ios="ios-menu" md="md-menu"></ion-icon>\n    </button>\n    <ion-title>Complaint Action List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-item>\n      <h2><b>\n        Trans# <ion-badge color="danger">171009</ion-badge>  \n        Batch# <ion-badge color="danger">6718716</ion-badge>\n      </b></h2>\n      <p>November 5, 1955</p>\n    </ion-item>\n\n    <ion-card-content>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Varietas</h2><ion-badge color="light">Bawang Mereh</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>Type</h2><ion-badge color="light">Domestik</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Valid#</h2><ion-badge color="light">-</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>CS Name</h2><ion-badge text-wrap text-left color="light">Amrullah Ali</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Status</h2>\n          <ion-badge text-wrap text-left color="light">\n            Waiting Approval by Agus Yulianto Waiting Approval by Agus Yulianto\n          </ion-badge>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n\n    <ion-row style="margin-top: -8px">\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="showWorkFlow()">\n          <ion-icon ios="ios-list" md="md-list"></ion-icon> Flows\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="showDetail()">\n          <ion-icon name=\'eye\'></ion-icon> Details\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="actionGo()"> \n          <ion-icon name="flash"></ion-icon> Actions\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-card>\n    <ion-item>\n      <h2>\n        Trans# <ion-badge color="danger">171009</ion-badge>  \n        Batch# <ion-badge color="danger">6718716</ion-badge>\n      </h2>\n      <p>November 5, 1955</p>\n    </ion-item>\n\n    <ion-card-content>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Varietas</h2><ion-badge color="light">Bawang Mereh</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>Type</h2><ion-badge color="light">Domestik</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Valid#</h2><ion-badge color="light">Valid External</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>CS Name</h2><ion-badge text-wrap text-left color="light">Amrullah Ali</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Status</h2>\n          <ion-badge text-wrap text-left color="light">\n            Waiting Approval by Agus Yulianto Waiting Approval by Agus Yulianto\n          </ion-badge>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n\n    <ion-row style="margin-top: -8px">\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon ios="ios-list" md="md-list"></ion-icon> Flows\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon name=\'eye\'></ion-icon> Details\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon name="flash"></ion-icon> Actions\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  \n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/actionlist/actionlist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]])
], ActionlistPage);

//# sourceMappingURL=actionlist.js.map

/***/ }),

/***/ 147:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 147;

/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/actionlist/actionlist.module": [
		582,
		1
	],
	"../pages/flows/flows.module": [
		581,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 190;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ApprovedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ApprovedPage = (function () {
    function ApprovedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ApprovedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApprovedPage');
    };
    return ApprovedPage;
}());
ApprovedPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-approved',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/approved/approved.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Approval</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-item padding>\n    <ion-label>Select Action</ion-label>\n    <ion-select>\n      <ion-option value="approve">Approve</ion-option>\n      <ion-option value="reject">Reject</ion-option>\n      <ion-option value="revised">Revised</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <div padding>\n    <ion-label color="dark">Note</ion-label>\n    <textarea rows="7" style="width: 100%;"></textarea>\n  </div>\n  \n  <div padding>\n    <button ion-button type="submit" color="danger" block>Submit</button>        \n  </div>  \n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/approved/approved.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], ApprovedPage);

//# sourceMappingURL=approved.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidationNotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ValidationNotePage = (function () {
    function ValidationNotePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ValidationNotePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ValidationNotePage');
    };
    return ValidationNotePage;
}());
ValidationNotePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-validation-note',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/validation-note/validation-note.html"*/'<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Validation</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n	<ion-item padding>\n    <ion-label>Select Action</ion-label>\n    <ion-select>\n      <ion-option value="invalid">Invalid</ion-option>\n      <ion-option value="v_internal">Valid Internal</ion-option>\n      <ion-option value="v_eksternal">Valid Eksternal</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item padding>\n    <ion-label fixed>CPA#</ion-label>\n    <ion-input type="text"></ion-input>\n  </ion-item>\n  <div padding>\n      <ion-label color="dark">Coment</ion-label>\n      <textarea rows="5" style="width: 100%;"></textarea>\n  </div>\n  <div padding>\n	    <ion-label color="dark">Proposed Replacement</ion-label>\n	    <textarea rows="5" style="width: 100%;"></textarea>\n  </div>\n  <div padding>\n    <button ion-button type="submit" color="danger" block>Submit</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/validation-note/validation-note.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], ValidationNotePage);

//# sourceMappingURL=validation-note.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateComplaintNamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_complaint_investigation_create_complaint_investigation__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CreateComplaintNamePage = (function () {
    function CreateComplaintNamePage(navCtrl, navParams, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.urlApi = "http://192.168.105.20:8080";
        this.country = 96;
        this.http = http;
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.getDataAPI();
        // this.onChange(96);
    }
    CreateComplaintNamePage.prototype.getDataAPI = function () {
        var _this = this;
        // Modal 2
        this.http.get(this.urlApi + '/nationality').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.nationalitys = data;
        });
        this.http.get(this.urlApi + '/user/mo').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.marketofs = data;
        });
        // Item
        this.http.get(this.urlApi + '/dataitem').map(function (res) { return res.json(); }).subscribe(function (datas) {
            _this.dataitems = datas;
        });
        // this.loading.present();
        // // countrys
        // this.http.get('http://shared.panahmerah.id/complaint/php/function/LoadComboData.php?nParam=4&nParentID=0&cText=').map(res => res.json()).subscribe(data => {
        //      this.countrys = data.data;
        //  }, 
        //  err => {
        //  	console.log("Error : " + err);
        //  },
        //  () => {
        //  	this.loading.dismiss();
        //  });
    };
    CreateComplaintNamePage.prototype.getDtMo = function (moId) {
        var _this = this;
        console.log(moId);
        this.http.get(this.urlApi + '/user/mo/' + moId.nEmployeeID).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.modatas = data[0].cUserEmail;
        });
    };
    CreateComplaintNamePage.prototype.getProv = function (countryID) {
        var _this = this;
        this.http.get(this.urlApi + '/nationalitylevel/' + countryID).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.provinces = data;
        });
    };
    CreateComplaintNamePage.prototype.getReg = function (provID) {
        var _this = this;
        this.http.get(this.urlApi + '/nationalityarea/' + provID).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.regrencys = data;
        });
    };
    CreateComplaintNamePage.prototype.keComplainNavigation = function () {
        //kirim variabel
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__create_complaint_investigation_create_complaint_investigation__["a" /* CreateComplaintInvestigationPage */], {
            ntransnumber: this.navParams.get('ntransnumber'),
            TxtDate: this.navParams.get('TxtDate'),
            CmbCsName: this.navParams.get('CmbCsName'),
            TxtBatch: this.navParams.get('TxtBatch'),
            TxtExpired: this.navParams.get('TxtExpired'),
            CmbComplaintType: this.navParams.get('CmbComplaintType'),
            CmbCrop: this.navParams.get('CmbCrop'),
            CmbVarietas: this.navParams.get('CmbVarietas'),
            CmbQty: this.navParams.get('CmbQty'),
            TxtQty: this.navParams.get('TxtQty'),
            CmbContent: this.navParams.get('CmbContent'),
            TxtContent: this.navParams.get('TxtContent'),
            TxtInspName: this.momarket.cUserName,
            TxtInspPhone: this.InspPhone,
            TxtInspMail: this.modatas,
            TxtCustName: this.CustName,
            TxtCustPhone: this.CustPhone,
            CmbCustCountry: this.nationality,
            CmbCustProvince: this.province,
            CmbCustRegency: this.regrency,
            TxtCustAddress: this.address
        });
    };
    return CreateComplaintNamePage;
}());
CreateComplaintNamePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-complaint-name',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/create-complaint-name/create-complaint-name.html"*/'<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Next New Complaint</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<h6 style="text-align: center;">Marketing Official</h6>\n\n	<ion-item>\n	    <ion-label color="dark">Name</ion-label>\n	    <ion-select col-9 [(ngModel)]="momarket" (ngModelChange)="getDtMo($event)">\n		    <ion-option *ngFor="let item of marketofs" [value]="item">{{item.cUserName}}</ion-option>\n	    </ion-select>\n	</ion-item>\n\n	<ion-item>\n	    <ion-label color="dark" stacked>Phone</ion-label>\n	    <ion-input [(ngModel)] = "InspPhone" name="InspPhone" type="number" placeholder=""></ion-input>\n	</ion-item>\n\n	<ion-item>\n	    <ion-label color="dark" stacked>Email</ion-label>\n	    <ion-input [(ngModel)] = "modatas" name="modatas" type="email" placeholder=""></ion-input>\n	</ion-item>\n\n	<br>\n	<h6 style="text-align: center;">Customer</h6>\n\n	<ion-item>\n	    <ion-label color="dark" stacked>Name</ion-label>\n	    <ion-input [(ngModel)] = "CustName" name="CustName"  type="text" placeholder=""></ion-input>\n	</ion-item>\n\n	<ion-item>\n	    <ion-label color="dark" stacked>Phone</ion-label>\n	    <ion-input [(ngModel)] = "CustPhone" name="CustPhone" type="number" placeholder=""></ion-input>\n	</ion-item>\n\n	<ion-item>\n		<ion-label color="dark" stacked>Address</ion-label>\n		<ion-textarea [(ngModel)] = "address" name="address"></ion-textarea>\n	</ion-item>\n\n	<ion-item>\n	    <ion-label color="dark">Country</ion-label>\n	    <ion-select col-9 [(ngModel)]="nationality" (ngModelChange)="getProv($event)">\n		    <ion-option *ngFor="let item of nationalitys" value="{{item.nNationalityID}}">{{item.cNationalityName}}</ion-option>\n	    </ion-select>\n	</ion-item>\n\n	<ion-item> \n    	<ion-label color="dark">Province</ion-label>\n    	<ion-select col-9 [(ngModel)]="province" (ngModelChange)="getReg($event)">\n	    	<ion-option *ngFor="let item of provinces" value="{{item.nNationalityLevelID}}">{{item.cNationalityLevelName}}</ion-option>\n		</ion-select>\n	</ion-item>\n\n	<ion-item>\n	    <ion-label color="dark">Regency</ion-label>\n	    <ion-select col-9 [(ngModel)]="regrency">\n		    <ion-option *ngFor="let item of regrencys" value="{{item.nAreaID}}">{{item.cAreaName}}</ion-option>\n	    </ion-select>\n	</ion-item>\n\n	<br>\n	<div>\n	    <button ion-button color="danger" type="submit" block (click) = "keComplainNavigation()">Next</button>\n	</div>\n	\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/create-complaint-name/create-complaint-name.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], CreateComplaintNamePage);

//# sourceMappingURL=create-complaint-name.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateComplaintInvestigationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CreateComplaintInvestigationPage = (function () {
    function CreateComplaintInvestigationPage(navCtrl, navParams, http, authService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.authService = authService;
        this.urlApi = "http://192.168.105.20:8080";
        this.filesToUpload = [];
        this.getDataitems();
        this.getDataUser();
    }
    CreateComplaintInvestigationPage.prototype.getDataUser = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.employID = profile.user.nEmployeeID;
            _this.dtEmploy = profile.user;
        });
    };
    CreateComplaintInvestigationPage.prototype.getDataitems = function () {
        var _this = this;
        // Item
        this.http.get(this.urlApi + '/dataitem').map(function (res) { return res.json(); }).subscribe(function (datas) {
            _this.dataitems = datas;
        });
    };
    CreateComplaintInvestigationPage.prototype.upload = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["RequestOptions"]({ headers: headers });
        var formData = new FormData();
        var files = this.filesToUpload;
        for (var i = 0; i < files.length; i++) {
            formData.append("uploads[]", files[i], files[i]['name']);
        }
        // formData.append("uploads[]", files[0], files[0]['name']);
        //this.address.documents = files.toString();
        var datafile = {
            nTransactionID: this.navParams.get('ntransnumber'),
            dProcessDate: this.navParams.get('TxtDate')
        };
        this.http.post(this.urlApi + '/transnum', datafile, options)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
        });
        this.http.post(this.urlApi + '/upload', formData, 'xxx')
            .map(function (res) { return res.json(); })
            .subscribe(function (datas) {
            console.log(datas[0]);
            _this.filename = datas;
        });
    };
    CreateComplaintInvestigationPage.prototype.fileChangeEvent = function (fileInput) {
        this.filesToUpload = fileInput.target.files;
        this.upload();
        //this.product.photo = fileInput.target.files[0]['name'];
    };
    CreateComplaintInvestigationPage.prototype.onSubmitComplaint = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["RequestOptions"]({ headers: headers });
        // let data = {
        // 	TxtEmployeeID : 1,
        // 	  TxtNumber : this.navParams.get('ntransnumber'),
        //      TxtDate : this.navParams.get('TxtDate'),
        //      CmbCsName : this.navParams.get('CmbCsName'),
        //      TxtBatch : this.navParams.get('TxtBatch'),
        //      TxtExpired :this.navParams.get('TxtExpired'),
        //      CmbComplaintType : this.navParams.get('CmbComplaintType'),
        //      CmbCrop : this.navParams.get('CmbCrop'),
        //      CmbVarietas : this.navParams.get('CmbVarietas'),
        //      CmbQty : this.navParams.get('CmbQty'),
        //      TxtQty : this.navParams.get('TxtQty'),
        //      CmbContent : this.navParams.get('CmbContent'),
        //      TxtContent : this.navParams.get('TxtContent'),
        //      TxtInspName : this.navParams.get('TxtInspName'), 
        //      TxtInspPhone : this.navParams.get('TxtInspPhone'), 
        //      TxtInspMail : this.navParams.get('TxtInspMail'), 
        //      TxtCustName : this.navParams.get('TxtCustName'), 
        //      TxtCustPhone : this.navParams.get('TxtCustPhone'),  
        //      CmbCustCountry : this.navParams.get('CmbCustCountry'), 
        //      CmbCustProvince :  this.navParams.get('CmbCustProvince'),
        //      CmbCustRegency : this.navParams.get('CmbCustRegency'),
        //      TxtCustAddress : this.navParams.get('TxtCustAddress'),
        //      TxtInvestigationDetail : this.complaintDetail,
        //      TxtInvestigationVisit : this.visitResult
        // };
        console.log(this.dtEmploy);
        var datatransheader = {
            dtemploy: this.dtEmploy,
            dtrans: {
                nTransactionID: this.navParams.get('ntransnumber'),
                nEmployeeID: this.employID,
                nEmployeeCSID: this.navParams.get('CmbCsName'),
                cTransactionNo: this.navParams.get('ntransnumber'),
                dTransactionDate: this.navParams.get('TxtDate'),
                cCustomerName: this.navParams.get('TxtCustName'),
                cCustomerAddress: this.navParams.get('TxtCustAddress'),
                nCountry: this.navParams.get('CmbCustCountry'),
                nProvince: this.navParams.get('CmbCustProvince'),
                nCounty: this.navParams.get('CmbCustRegency'),
                cCustomerPhoneNo: this.navParams.get('TxtCustPhone'),
                cInspectorName: this.navParams.get('TxtInspName'),
                cInspectorPhoneNo: this.navParams.get('TxtInspPhone'),
                cInspectorEmail: this.navParams.get('TxtInspMail'),
                cTransactionDescription: this.complaintDetail,
                cTransactionFirstResult: this.visitResult,
                bTransactionStatus: 1,
                dInsertDate: this.navParams.get('TxtDate'),
            },
            // quantity detail
            detquantity: [
                {
                    nTransactionID: this.navParams.get('ntransnumber'),
                    nTransactionUnitID: this.navParams.get('CmbQty'),
                    nTransactionValue: this.navParams.get('TxtQty'),
                    dProcessDate: this.navParams.get('TxtDate'),
                },
                {
                    nTransactionID: this.navParams.get('ntransnumber'),
                    nTransactionUnitID: this.navParams.get('CmbContent'),
                    nTransactionValue: this.navParams.get('TxtContent'),
                    dProcessDate: this.navParams.get('TxtDate'),
                }
            ],
            // data item
            dataItems: {
                nTransactionID: this.navParams.get('ntransnumber'),
                nItemID: this.dataitems,
                cTransactionValue: [this.navParams.get('TxtBatch'),
                    this.navParams.get('TxtExpired'),
                    this.navParams.get('CmbCrop'),
                    this.navParams.get('CmbVarietas')
                ],
                cTransactionDetailNotes: null,
                dProcessDate: this.navParams.get('TxtDate')
            }
        };
        this.http.post(this.urlApi + '/complaintheader', datatransheader, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
        });
    };
    CreateComplaintInvestigationPage.prototype.ionViewDidLoad = function () {
    };
    return CreateComplaintInvestigationPage;
}());
CreateComplaintInvestigationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-complaint-investigation',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/create-complaint-investigation/create-complaint-investigation.html"*/'<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Next New Complaint</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<h6 style="text-align: center;">Investigation</h6>\n\n	<div padding>\n	    <ion-label color="dark">Complaint Detail</ion-label>\n	    <textarea rows="7" [(ngModel)] ="complaintDetail" name="complaintDetail" style="width: 100%;"></textarea>\n	</div>\n\n	<div padding>\n	    <ion-label color="dark">Visit Result</ion-label>\n	    <textarea rows="7" [(ngModel)] ="visitResult" style="width: 100%;"></textarea>\n  	</div>\n\n	<br>\n		<p style="text-align: center;">Upload Additional Information</p>\n	<br>\n\n	<label ion-button color="primary" id="cin" name="cin" (change)="fileChangeEvent($event)">\n          Upload File \n          <input id="cin" name="cin" type="file" style="display: none;">\n    </label>\n	<!-- <button ion-button color="primary">Upload File</button> -->\n\n	<br>\n	<br>\n\n	<button ion-button type="submit" color="danger" (click)="onSubmitComplaint()" block>Submit</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/create-complaint-investigation/create-complaint-investigation.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth__["a" /* Auth */]])
], CreateComplaintInvestigationPage);

//# sourceMappingURL=create-complaint-investigation.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_detail_list_detail_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_flows_flows__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListPage = (function () {
    function ListPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.filYear = new Date().getFullYear();
        this.filOption = 0;
    }
    ListPage.prototype.ionViewDidLoad = function () {
        //this.getListComplaint();
    };
    ListPage.prototype.getListComplaint = function () {
        var _this = this;
        this.http.get('http://shared.panahmerah.id/complaint/php/view/ViewGridListComplaint.php?nEmployeeID=1&nVwOption=0&nYear=2017&page=1&start=0&limit=10')
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.lisComplaint = data.data;
            console.log(_this.lisComplaint);
        });
    };
    ListPage.prototype.showDetail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_detail_list_detail_list__["a" /* DetailListPage */]);
    };
    ListPage.prototype.showWorkFlow = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_flows_flows__["a" /* FlowsPage */]);
    };
    return ListPage;
}());
ListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-list',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <button ion-button menuToggle>\n      <ion-icon ios="ios-menu" md="md-menu"></ion-icon>\n    </button>\n    <ion-title>Complaint List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-row style="padding: 0">\n      <ion-col col-4 col-auto>\n        <ion-item>\n          <ion-label small>Year</ion-label>\n          <ion-input type="number"  name="filYear" [(ngModel)]="filYear"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col col-8>\n        <ion-item>\n          <ion-label>Options</ion-label>\n          <ion-select col-8 name="filOption" [(ngModel)]="filOption">\n            <ion-option value=0>On Progress</ion-option>\n            <ion-option value=4>Completed</ion-option>\n            <ion-option value=3>Rejected</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-searchbar (ionInput)="getItems($event)" style="margin-top: -8px"></ion-searchbar>\n\n  <ion-card>\n    <ion-item>\n      <h2><b>\n        Trans# <ion-badge color="danger">171009</ion-badge>  \n        Batch# <ion-badge color="danger">6718716</ion-badge>\n      </b></h2>\n      <p>November 5, 1955</p>\n    </ion-item>\n\n    <ion-card-content>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Varietas</h2><ion-badge color="light">Bawang Mereh</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>Type</h2><ion-badge color="light">Domestik</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Valid#</h2><ion-badge color="light">-</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>CS Name</h2><ion-badge text-wrap text-left color="light">Amrullah Ali</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Status</h2>\n          <ion-badge text-wrap text-left color="light">\n            Waiting Approval by Agus Yulianto Waiting Approval by Agus Yulianto\n          </ion-badge>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n\n    <ion-row style="margin-top: -8px">\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="showWorkFlow()">\n          <ion-icon ios="ios-list" md="md-list"></ion-icon> Flows\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="showDetail()">\n          <ion-icon name=\'eye\'></ion-icon> Details\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left (click)="actionGo()">\n          <ion-icon name="create"></ion-icon> Edit\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-card>\n    <ion-item>\n      <h2>\n        Trans# <ion-badge color="danger">171009</ion-badge>  \n        Batch# <ion-badge color="danger">6718716</ion-badge>\n      </h2>\n      <p>November 5, 1955</p>\n    </ion-item>\n\n    <ion-card-content>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Varietas</h2><ion-badge color="light">Bawang Mereh</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>Type</h2><ion-badge color="light">Domestik</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Valid#</h2><ion-badge color="light">Valid External</ion-badge>\n        </ion-col>\n        <ion-col>\n          <h2>CS Name</h2><ion-badge text-wrap text-left color="light">Amrullah Ali</ion-badge>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top: -8px">\n        <ion-col>\n          <h2>Status</h2>\n          <ion-badge text-wrap text-left color="light">\n            Waiting Approval by Agus Yulianto Waiting Approval by Agus Yulianto\n          </ion-badge>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n\n    <ion-row style="margin-top: -8px">\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon ios="ios-list" md="md-list"></ion-icon> Flows\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon name=\'eye\'></ion-icon> Details\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="danger" clear small icon-left>\n          <ion-icon name="create"></ion-icon> Edit\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  \n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/list/list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"]])
], ListPage);

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogOutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LogOutPage = (function () {
    function LogOutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.onLogout();
    }
    LogOutPage.prototype.onLogout = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    return LogOutPage;
}());
LogOutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-logout',
        template: ''
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], LogOutPage);

//# sourceMappingURL=logout.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(272);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_list_list__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_createComplaint_create_complaint__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_create_complaint_name_create_complaint_name__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_create_complaint_investigation_create_complaint_investigation__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_logout_logout__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_providers_auth__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_detail_list_detail_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_approved_approved__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_flows_flows__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_actionlist_actionlist__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_distribution_select_distribution_select__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_investigation_comment_investigation_comment__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_validation_note_validation_note__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_cs_confirm_send_cs_confirm_send__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_status_bar__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__ = __webpack_require__(262);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_detail_list_detail_list__["a" /* DetailListPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_approved_approved__["a" /* ApprovedPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_flows_flows__["a" /* FlowsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_actionlist_actionlist__["a" /* ActionlistPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_logout_logout__["a" /* LogOutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_createComplaint_create_complaint__["a" /* CreateComplaintPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_create_complaint_name_create_complaint_name__["a" /* CreateComplaintNamePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_create_complaint_investigation_create_complaint_investigation__["a" /* CreateComplaintInvestigationPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_distribution_select_distribution_select__["a" /* DistributionSelectPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_investigation_comment_investigation_comment__["a" /* InvestigationCommentPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_validation_note_validation_note__["a" /* ValidationNotePage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_cs_confirm_send_cs_confirm_send__["a" /* CsConfirmSendPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/flows/flows.module#FlowsPageModule', name: 'FlowsPage', segment: 'flows', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/actionlist/actionlist.module#ActionlistPageModule', name: 'ActionlistPage', segment: 'actionlist', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_detail_list_detail_list__["a" /* DetailListPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_approved_approved__["a" /* ApprovedPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_flows_flows__["a" /* FlowsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_actionlist_actionlist__["a" /* ActionlistPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_logout_logout__["a" /* LogOutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_createComplaint_create_complaint__["a" /* CreateComplaintPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_create_complaint_name_create_complaint_name__["a" /* CreateComplaintNamePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_create_complaint_investigation_create_complaint_investigation__["a" /* CreateComplaintInvestigationPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_distribution_select_distribution_select__["a" /* DistributionSelectPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_investigation_comment_investigation_comment__["a" /* InvestigationCommentPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_validation_note_validation_note__["a" /* ValidationNotePage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_cs_confirm_send_cs_confirm_send__["a" /* CsConfirmSendPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_12__pages_providers_auth__["a" /* Auth */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 573:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_actionlist_actionlist__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_createComplaint_create_complaint__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_logout_logout__ = __webpack_require__(266);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        this.pages = [
            { icon: 'logo-windows', title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { icon: 'add', title: 'New Complaint', component: __WEBPACK_IMPORTED_MODULE_8__pages_createComplaint_create_complaint__["a" /* CreateComplaintPage */] },
            { icon: 'list', title: 'List Complaint', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] },
            { icon: 'checkbox', title: 'Action List Complaint', component: __WEBPACK_IMPORTED_MODULE_6__pages_actionlist_actionlist__["a" /* ActionlistPage */] },
            { icon: 'log-out', title: 'LogOut', component: __WEBPACK_IMPORTED_MODULE_9__pages_logout_logout__["a" /* LogOutPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/iqbal/panahmerah/src/app/app.html"*/'<ion-menu [content]="content" id="menu1">\n  <ion-header>\n    <div style="height: 175px; color: white;">\n      <div style="padding-top: 15px; padding-bottom: 0px">\n        <div style="text-align: center;">\n          <img src="assets/logo/logopm.png" style="height: 100px;" />\n        </div>\n      </div>\n      <h5 style="text-align: center; color: #262626;">Hallo Admin</h5>\n    </div>\n  </ion-header>\n \n  <ion-content padding>\n    <ion-list no-lines>\n      <button menuClose icon-left ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        <ion-icon name="{{p.icon}}"></ion-icon>{{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" swipeBackEnabled="false" #content ></ion-nav>'/*ion-inline-end:"/home/iqbal/panahmerah/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistributionSelectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DistributionSelectPage = (function () {
    function DistributionSelectPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DistributionSelectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DistributionSelectPage');
    };
    return DistributionSelectPage;
}());
DistributionSelectPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-distribution-select',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/distribution-select/distribution-select.html"*/'<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Select Distribution</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<ion-item padding>\n    <ion-label>Select Dept</ion-label>\n    <ion-select>\n      <ion-option value="0">QA MGR</ion-option>\n      <ion-option value="1">DEPT X</ion-option>\n    </ion-select>\n  </ion-item>\n	<ion-item padding>\n    <ion-label>Select Complaint Type</ion-label>\n    <ion-select>\n      <ion-option value="xx">xxxx</ion-option>\n      <ion-option value="cc">cc</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <div padding>\n    <ion-label color="dark">Note</ion-label>\n    <textarea rows="6" style="width: 100%;"></textarea>\n  </div>\n  \n  <div padding>\n    <button ion-button type="submit" color="danger" block>Submit</button>        \n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/distribution-select/distribution-select.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], DistributionSelectPage);

//# sourceMappingURL=distribution-select.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvestigationCommentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InvestigationCommentPage = (function () {
    function InvestigationCommentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InvestigationCommentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InvestigationCommentPage');
    };
    return InvestigationCommentPage;
}());
InvestigationCommentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-investigation-comment',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/investigation-comment/investigation-comment.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Investigation Comment</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div padding>\n	    <ion-label color="dark">Comment</ion-label>\n	    <textarea rows="7" style="width: 100%;"></textarea>\n  </div>\n  <div padding>\n    <button ion-button color="primary">Upload File</button>\n  </div>\n	<ion-item>namafile.jpg\n    <button ion-button clear item-end>\n      Delete\n    </button>\n  </ion-item>\n  <ion-item>namafile.jpg\n    <button ion-button clear item-end>\n      Delete\n    </button>\n  </ion-item>\n	\n  <div padding>\n    <button ion-button type="submit" color="danger" block>Submit</button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/investigation-comment/investigation-comment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], InvestigationCommentPage);

//# sourceMappingURL=investigation-comment.js.map

/***/ }),

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CsConfirmSendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CsConfirmSendPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CsConfirmSendPage = (function () {
    function CsConfirmSendPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CsConfirmSendPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CsConfirmSendPage');
    };
    return CsConfirmSendPage;
}());
CsConfirmSendPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cs-confirm-send',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/cs-confirm-send/cs-confirm-send.html"*/'<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Customer Service Confirm</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div >\n	<ion-row>\n            <ion-col>\n            </ion-col>\n            <ion-col>\n                <p>Date Send : 23-09-2017</p>\n            </ion-col>\n    </ion-row> \n\n    <ion-card>\n	    <ion-list radio-group [(ngModel)]="relationship">\n		  <ion-item>\n		    <ion-label>Compentation</ion-label>\n		    <ion-radio value="compentation"></ion-radio>\n		  </ion-item>\n		  <ion-item>\n		    <ion-label>No Compentation</ion-label>\n		    <ion-radio value="n_compentation"></ion-radio>\n		  </ion-item>\n		</ion-list>\n    </ion-card>\n\n    <ion-item>\n	    <ion-label color="dark" stacked>Complaint Number</ion-label>\n	    <ion-input type="number" placeholder="0000">234567i8</ion-input>\n	</ion-item>\n  	<ion-item>\n    	<ion-label color="dark" stacked>Invace Number</ion-label>\n    	<ion-input type="number" placeholder="0000">234567i8</ion-input>\n  	</ion-item>\n  	\n  	<div padding>\n	    <ion-label color="dark">Comment</ion-label>\n	    <textarea rows="7" style="width: 100%;"></textarea>\n  	</div>\n  	<br>\n  	<ion-item>\n    	<ion-label color="dark">Select Varieties</ion-label>\n    	<ion-select>\n	    	<ion-option value="padi">PADI</ion-option>\n	    	<ion-option value="jagung">JAGUNG</ion-option>\n	    	<ion-option value="bawang">BAWANG MERAH</ion-option>\n	    </ion-select>\n	</ion-item>\n  </div>\n  <br>\n  <br>	\n  <button ion-button type="submit" color="danger" block>Submit</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/cs-confirm-send/cs-confirm-send.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], CsConfirmSendPage);

//# sourceMappingURL=cs-confirm-send.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FlowsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FlowsPage = (function () {
    function FlowsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FlowsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FlowsPage');
    };
    return FlowsPage;
}());
FlowsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-flows',template:/*ion-inline-start:"/home/iqbal/panahmerah/src/pages/flows/flows.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Workflow History</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n	<ion-item>\n		<h2 text-wrap><b>Phase Approval by Arizal</b></h2>\n		<h2>\n			Status <ion-badge color="dark">Waiting</ion-badge>\n			Approval <ion-badge color="dark">Approved</ion-badge>\n		</h2>	\n		<p text-wrap>Ini coment dari approval</p>\n		<ion-note item-end>\n			<ion-badge color="light">\n				<h2 style="color: red">1</h2>\n			</ion-badge>\n		</ion-note>\n	</ion-item>\n	<ion-item>\n		<h2 text-wrap><b>Phase Approval by Arizal</b></h2>\n		<h2>\n			Status <ion-badge color="dark">Waiting</ion-badge>\n			Approval <ion-badge color="dark">Approved</ion-badge>\n		</h2>	\n		<p text-wrap>Ini coment dari approval</p>\n		<ion-note item-end>\n			<ion-badge color="light">\n				<h2 style="color: red">2</h2>\n			</ion-badge>\n		</ion-note>\n	</ion-item>\n</ion-content>\n'/*ion-inline-end:"/home/iqbal/panahmerah/src/pages/flows/flows.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], FlowsPage);

//# sourceMappingURL=flows.js.map

/***/ })

},[267]);
//# sourceMappingURL=main.js.map